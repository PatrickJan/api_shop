<?php

namespace Calc;

final class QueryData
{
    private $defaultPage = 'heartbeat';
    private $defaultAction = 'none';

    public $page;
    public $action;
    public $method;
    public $params = array();
    public $authToken ='';


    public function __construct()
    {
        $this->clientIp = $_SERVER['REMOTE_ADDR'];

        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $uri = array_shift($uri);
        $requestParams = explode('/', $uri);
        $scriptPath = explode('/', $_SERVER['SCRIPT_NAME']);
        $this->method = $_SERVER['REQUEST_METHOD'];

        // remove the base path
        while ($requestParams[0] === $scriptPath[0]) {
            array_shift($requestParams);
            array_shift($scriptPath);
        }

        if ((count($requestParams) > 0) && ($requestParams[0] != '')) {
            $this->page = array_shift($requestParams);
        } else {
            $this->page = $this->defaultPage;
        }
        if ((count($requestParams) > 0) && ($requestParams[0] != '')) {
            $this->action = array_shift($requestParams);
        } else {
            $this->action = $this->defaultAction;
        }
        foreach ($_GET as $k => $v) {
            $this->params[$k] = $v;
        }
        foreach ($_POST as $k => $v) {
            $this->params[$k] = $v;
        }
        $postdata = file_get_contents("php://input");
        if ($postdata) {
            $request = json_decode($postdata);
            foreach ($request as $k => $v) {
                $this->params[$k] = $v;
            }
        }

        while (count($requestParams) > 1) {
            $k = array_shift($requestParams);
            $v = $requestParams[0];
            if (($k != '') && !is_numeric($k)) {
                $this->params[$k] = $v;
            }
        }

        $headers = getallheaders();
        if (isset($headers['Authorization']) && (substr($headers['Authorization'], 0, 7) == "Bearer ")) {
            $this->authToken = substr($headers['Authorization'], 7);
        }
    }
}
