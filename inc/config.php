<?php

namespace Calc;

final class LoginData
{
    //API
    public static $baseurl = "";
    public static $username = "";
    public static $passwd = "";

    //DB
    public static $dbHost = "";
    public static $dbName = "";
    public static $dbUser = "";
    public static $dbPasswd = "";

    //Settings
    public static $sessionTime = 100; //Minutes;
    public static $dataStoreTime = 30; //Minutes;
}
