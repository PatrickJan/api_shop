<?php
namespace Calc;

require_once("config.php");
require_once("Singleton.php");

use Singleton\Singleton;
use Calc\LoginData;
use Exception;

final class PlentyMarkets extends Singleton
{
    private $baseUrl;
    private $authToken;
    private $expire;
    private $refreshToken;

    public function __construct()
    {
        $this->baseUrl = LoginData::$baseurl;

        $data = $this->loginPlenty(LoginData::$username, LoginData::$passwd);
        if ($data) {
            $this->authToken = $data["token_type"]." ".$data["access_token"];

            if ($this->authToken == false) {
                throw new Exception("authToken missing?");
            }
        } else {
            throw new \Exception("Login Wrong");
        }

        $this->expire = $data["expiresIn"];
        $this->refreshToken = $data["refresh_token"];
    }

    private function loginPlenty($username, $passwd) //AuthLogin
    {
        $url = "/rest/login";
        $curlObj = curl_init();

        $options = [
            CURLOPT_URL => $this->baseUrl.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_AUTOREFERER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS =>
                array("username"=>$username, "password"=>$passwd)
        ];

        curl_setopt_array($curlObj, $options);

        if (curl_error($curlObj) == "") {
            $returnData = json_decode(curl_exec($curlObj), true);

            return $returnData;
        } else {
            return false;
        }
    }

    public function getPlentyAnswer($url, $type)
    {
        $curlObj = curl_init();

        $options = [
            CURLOPT_URL => $this->baseUrl."".$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_AUTOREFERER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization:".$this->authToken.""
              ),
        ];

        curl_setopt_array($curlObj, $options);
        $returnData = json_decode(curl_exec($curlObj), true);

        return $returnData;
    }

    public function vkPreise(string $searchkey = "", int $start = 0, int $count = 20)
    {
        
        $retunTempData = $this->getPlentyAnswer("/rest/items?name=%".$searchkey."%&page=".$start."&itemsPerPage=".$count, "GET");

        $retunData = array("total" => $retunTempData["totalsCount"],
                           "start"=> $retunTempData["firstOnPage"],
                           "count"=> $retunTempData["lastOnPage"],
                           "rows" => array());

        foreach ($retunTempData["entries"] as $entries) {
            $retunData["rows"][] = ["id"=>$entries["id"],
                                    "name"=>$entries["texts"][0]["name1"],
                                    "costPresetId" => "X",
                                    "voucherPresetId" => "X",
                                    "productCount" => "X"];
        }
        //TBD

        return array($retunData, null);
    }

    public function vkGroups()
    {
        $returnTempData = $this->getPlentyAnswer("/rest/categories", "GET");
        
        $ignore = array(182, 183, 184, 185, 186, 187, 188, 189, 190, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 210);
        $returnData = array("total" => ($returnTempData["totalsCount"] - count($ignore)), "groups" => array());

        foreach ($returnTempData["entries"] as $entries) {
            if (!in_array($entries["id"], $ignore)) {
                $returnData["groups"][] = ["id" => $entries["id"],
                                           "plentyGroupId" => $entries["details"][0]["plentyId"],
                                           "name" => $entries["details"][0]["name"],
                                           "parentId" => $entries["parentCategoryId"],
                                           "childGroups" => array()
                                        ];
            }
        }

        $copyData = $returnData["groups"];
        $i = 0;
        foreach ($copyData as $groups) {
            $j = 0;
            foreach ($copyData as $groups2) {
                if ($groups["id"] == $groups2["parentId"]) {
                    $returnData["groups"][$i]["childGroups"][] = ["id" => $groups2["id"],
                                                                  "plentyGroupId" => $groups2["plentyGroupId"],
                                                                  "name" => $groups2["name"],
                                                                  "parentId" => $groups2["parentId"],
                                                                  "childGroups" => array()
                    ];
                    unset($returnData["groups"][$j]);
                }
                $j++;
            }
            $i++;
        }
        $returnData["groups"] = array_values($returnData["groups"]);
        return json_encode($returnData);
    }

    public function getOneItem(int $itemId)
    {
        if ($itemId != 0) {
            $return = $this->getPlentyAnswer("/rest/items/".$itemId, "GET");
            
            if (array_key_exists("error", $return)) {
                return array(null, array("err_nr"=>"404", "err_msg" => "number not found"));
            } else {
                //$returnData = array("id" => $return["id"], );
                return array($return, null);
            }
        } else {
            return array(null,array("err_nr"=>"405", "err_msg" => "unknow number"));
        }
    }

    public function userLogoff()
    {
        $this->getPlentyAnswer("/rest/logout", "POST");
    }

    public function heartbeat()
    {
        $this->getPlentyAnswer("/rest/login/refresh", "POST");
    }
}
