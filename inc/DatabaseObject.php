<?php
namespace Calc;

require_once("config.php");
require_once("Singleton.php");
require_once("PlentyMarkets.php");

use Singleton\Singleton;
use Calc\LoginData;
use Calc\PlentyMarkets;
use \PDO;
use \PDOException;
use \PDOStatement;

$pm = PlentyMarkets::instance();

final class DatabaseObject extends Singleton
{

    private $dbHandler;

    public function __construct()
    {
        try {
            $this->dbHandler = new PDO(
                "mysql:host=".LoginData::$dbHost.";dbname=".LoginData::$dbName.";charset=utf8",
                LoginData::$dbUser,
                LoginData::$dbPasswd,
                array(PDO::ATTR_ERRMODE => true,
                      PDO::ERRMODE_EXCEPTION => true,
                      PDO::ATTR_EMULATE_PREPARES => false)
            );
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        $this->cleanOldUser();
    }

    //prepare with array(args)| bool single for fetch instead fetchall
    private function prepareDB($query, array $args = null, $single = false)
    {
        if (isset($args) && is_array($args)) {
            $stmt = $this->dbHandler->prepare($query);
            $stmt->execute($args);
    
            if ($single) {
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            throw new \Exception("prepareDB needs $args to be an working array");
        }


        return $result;
    }

    //execute without any array| bool single for fetch instead of fetchall
    private function executeDB($query, $single = false)
    {
        $stmt = $this->dbHandler->prepare($query);
        $stmt->execute();

        if ($single) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return $result;
    }

    //just send some data without return
    private function noreturnDB($query, array $args = null)
    {
        if (is_array($args)) {
            $this->prepareDB($query, $args);
        } else {
            $this->executeDB($query);
        }
    }

    private function generateToken($username)
    {
        $token = md5(uniqid($username, true));
        return $token;
    }

    private function cleanOldUser()
    {
        $this->noreturnDB("DELETE FROM user_session WHERE sessionStop NOT 
        BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL ".LoginData::$sessionTime." MINUTE)");
    }

    //login
    public function loginUser($username, $password)
    {
        $return = $this->prepareDB(
            "SELECT userID, userName, screenName, email FROM user_data WHERE 
            userName = :userName AND userPassword = :userPassword",
            array(":userName" => $username, ":userPassword" => $password),
            true
        );

        if ($return) {
            $token = $this->generateToken($username);

            $this->noreturnDB(
                "INSERT INTO user_session (userID, sessionStart, sessionStop, token, ip) 
                VALUES (:userID ,NOW(), DATE_ADD(NOW(),INTERVAL ".LoginData::$sessionTime." MINUTE),:token, :ip)
                ON DUPLICATE KEY UPDATE token = :ntoken, sessionStop = 
                DATE_ADD(NOW(),INTERVAL ".LoginData::$sessionTime." MINUTE)",
                array(":userID" => $return["userID"],":token" => $token,
                ":ntoken" => $token, ":ip" => $_SERVER["REMOTE_ADDR"])
            );

            return array(array("userId" => $return["userID"], "userName" => $return["userName"],
            "screenName" => $return["screenName"], "email" => $return["email"], "sessionToken" =>
            $token, "permissions" => "2" ), null);
        } else {
            return array(null, array("err_nr" => "400", "err_msg" => "wrong initials"));
        }
    }

    public function logoffUser($token)
    {
        $this->noreturnDB("DELETE FROM user_session WHERE token = :token", array(":token" => $token));
        $this->cleanOldUser();

        return array(null, array("err_nr" => null, "err_msg" => ""));
    }

    public function refreshUser($token)
    {
        $return = $this->prepareDB("SELECT user_session.userID, userName, screenName, email FROM user_data 
        LEFT JOIN user_session ON user_data.userID = user_session.userID 
        WHERE token = :token", array(":token" => $token), true);

        if ($return) {
            $this->noreturnDB("UPDATE user_session SET sessionStop = 
            DATE_ADD(NOW(),INTERVAL ".LoginData::$sessionTime." MINUTE) 
            WHERE token = :token ", array(":token" => $token));

            return array(array("userId" => $return["userID"], "userName" => $return["userName"],
            "screenName" => $return["screenName"], "email" => $return["email"], "sessionToken" =>
            $token, "permissions" => "2" ), null);
        } else {
            return array(null, array("err_nr" => "408", "err_mgs" => "session expired"));
        }
    }

    public function checkValidUser($token)
    {
        $return = $this->prepareDB("SELECT COUNT(*) AS number FROM user_session 
        WHERE token = :token", array(":token"=>$token), true);
        
        if ($return["number"] === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function refreshJsonDB()
    {
        $this->noreturnDB("DELETE FROM plenty_data_dump WHERE dataTimestamp NOT 
        BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL ".LoginData::$dataStoreTime." MINUTE)");
    }

    public function saveJsonToDB(string $name, $json)
    {
        $sql = "INSERT INTO plenty_data_dump (name, dataTimestamp, dataDump) VALUES
        (:name, DATE_ADD(NOW(),INTERVAL ".LoginData::$dataStoreTime." MINUTE),:json)
        ON DUPLICATE KEY UPDATE dataTimestamp = DATE_ADD(NOW(),INTERVAL ".LoginData::$dataStoreTime." MINUTE),
        dataDump = :json2";

        $this->noreturnDB($sql, array(":name"=>$name, ":json"=>$json,  ":json2"=>$json));
    }

    public function getJsonFromDB(string $name)
    {
        $this->refreshJsonDB();

        $sql = "SELECT dataDump FROM plenty_data_dump WHERE name = :name AND 
        dataTimestamp BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL ".LoginData::$dataStoreTime." MINUTE)";

        $return = $this->prepareDB($sql, array(":name"=>$name), true);

        if ($return) {
            return array($return, null);
        } else {
            return array(null, array("err_nr" => "505", "err_mgs" => "no data found"));
        }
    }

    public function saveOneItemToDB($id)
    {
        global $pm;
        $data = $pm->getOneItem($id);

        if (!array_key_exists("error", $data)) {
            $this->saveJsonToDB($id, json_encode($data));
        }
    }

    #URL: "/calc"
    public function getAllCalc(string $searchkey = "", int $start = 0, int $count = 20)
    {
        if ($searchkey != "") {
            $sql = "SELECT * FROM calc_info WHERE name like concat('%',:search,'%') LIMIT :min, :max";
            $return = $this->prepareDB($sql, array(":search"=>$searchkey,":min"=>$start,":max"=>$count));
        } else {
            $sql = "SELECT * FROM calc_info LIMIT :min, :max";
            $return = $this->prepareDB($sql, array(":min"=>$start,":max"=>$count));
        }

        
        $total = $this->executeDB("SELECT COUNT(*) AS COUNT FROM calc_info", true);

        $returnData = array("total"=> $total["COUNT"], "start" => $start, "count" => $count, "rows" => array());

        foreach ($return as $entries) {
            $returnData["rows"][] = ["id"=>$entries["id"],
                                    "name"=>$entries["name"],
                                    "costPresetId" => $entries["costPresetId"],
                                    "voucherPresetId" => $entries["voucherPresetId"],
                                    "productCount" => "X"];
        }
        return array($returnData, null);
    }

    #URL: "/calc/add"
    public function addOneCalc(string $name, int $costPresetId = null, int $voucherPresetId = null)
    {
        $this->prepareDB("INSERT INTO calc_info (name, costPresetId, voucherPresetId) 
        VALUES (:name, :costPre, :vouchPre)", array(":name"=>$name,":costPre"=>$costPresetId,
        ":vouchPre"=>$voucherPresetId));

        $return = $this->prepareDB("SELECT * FROM calc_info WHERE name = :name", array(":name" => $name), true);

        $returnData = array("id"=>$return["id"], "name"=>$return["name"],
        "costPresetId"=>$return["costPresetId"],"voucherPresetId"=>$return["voucherPresetId"]);

        return array($returnData, null);
    }

    #URL: "/calc/%id%"
    public function detailOneCalc(int $id)
    {
        $return = $this->prepareDB("SELECT calc_info.id, calc_info.name, costPresetId, voucherPresetId,
        count(calc_products.calc_id) AS productCount FROM calc_info  LEFT JOIN calc_products ON calc_info.id 
        = calc_products.calc_id WHERE calc_info.id = :id", array(":id"=>$id), true);

        if (!empty($return["id"])) {
            $return["costPresetId"] != null ?
                $return["costPresetId"] = $this->prepareDB("SELECT * FROM calc_cost 
                WHERE id = :id", array(":id"=>$return["costPresetId"])):
                $return["costPresetId"] = array();
            $return["voucherPresetId"] != null ?
                $return["voucherPresetId"] = $this->prepareDB("SELECT * FROM calc_voucher 
                WHERE id = :id", array(":id"=>$return["voucherPresetId"])):
                $return["voucherPresetId"] = array();

            return array($return, null);
        } else {
            return array(null, array("err_nr"=>"410", "err_msg" => "unknown id"));
        }
    }

    #URL: "/calc/%id%/update"
    public function updateOneCalc(int $id, string $name, int $costPresetId = null, int $voucherPresetId = null)
    {
        $this->prepareDB("UPDATE calc_info SET name=:name, costPresetId = :costPre, voucherPresetId = :vouchPre 
        WHERE id = :id", array(":name"=>$name, ":costPre"=>$costPresetId, ":vouchPre"=>$voucherPresetId, ":id"=>$id));
        $return = $this->prepareDB("SELECT * FROM calc_info WHERE id = :id", array(":id"=>$id), true);

        if (!empty($return)) {
            $returnData = array("id"=>$return["id"], "name"=>$return["name"],
            "costPresetId"=>$return["costPresetId"],"voucherPresetId"=>$return["voucherPresetId"]);

            return array($returnData, null);
        } else {
            return array(null, array("err_nr"=>"420", "err_msg" => "unknown id"));
        }
    }

    #URL: "/calc/%id%/delete"
    public function deleteOneCalc(int $id)
    {
        $this->noreturnDB("DELETE FROM calc_info WHERE id = :id", array(":id"=>$id));
        return array(null, null);
    }

    #URL: "/calc/%id%/products"
    public function getAllProductsOneCalc(int $id, string $searchkey = "", int $start = 0, int $count = 20)
    {
        if ($searchkey != "") {
            $sql = "SELECT * FROM calc_products WHERE calc_id = :id AND product_name like concat('%',:search,'%') 
            LIMIT :min, :max";
            $return = $this->prepareDB($sql, array(":id"=>$id,":search"=>$searchkey,":min"=>$start,":max"=>$count));
        } else {
            $sql = "SELECT * FROM calc_products WHERE calc_id = :id LIMIT :min, :max";
            $return = $this->prepareDB($sql, array(":id"=>$id, ":min"=>$start, ":max"=>$count));
        }

        
        $total = $this->prepareDB("SELECT COUNT(*) AS COUNT FROM calc_products WHERE 
        calc_id = :id", array(":id"=>$id), true);

        $returnData = array("total"=> $total["COUNT"], "start" => $start, "count" => $count, "rows" => array());

        //TBD
        foreach ($return as $entries) {
            $returnData["rows"][] = ["name"=>$entries["product_name"],
                                    "productCount" => "X"];
        }

        return array($returnData, null);
    }

    #URL: "/calc/%id%/products/add"
    public function addOneProductOneCalc(int $id, int $productId)
    {
        if ($productId != 0) {
            $sql = "INSERT INTO calc_products (calc_id, plentyId) VALUES (:id, :plentyId)";
            $this->prepareDB($sql, array(":id"=>$id,":plentyId"=>$productId));
        } else {
            return array(null,array("err_nr"=>"401", "err_mgs" => "plentyId not a correct number"));
        }
    }
    
    public function detailOneProduct(int $id, int $productId)
    {
        //TBD
        return array(array("sucess"=>"123","msg"=>"TBD"), null);
    }

    public function delteOneProduct(int $id, int $productId)
    {
        //TBD
        return array(array("sucess"=>"123","msg"=>"TBD"), null);
    }

    #URL: "/calc/products"
    public function getAllProductPrice(string $name, int $start, int $count)
    {
        global $pm;
        $product = $pm->vkPreise($name, $start, $count);

        return $product;
    }

    #URL: "/calc/productGroups"
    public function getAllGroups()
    {
        $return = $this->getJsonFromDB("groups");

        if ($return[1]["err_nr"] != null) {
            global $pm;
            $groups = $pm->vkGroups();

            $this->saveJsonToDB("groups", $groups);
            $return = $this->getJsonFromDB("groups");
        }
        
        return array(json_decode($return[0]["dataDump"], true), null);
    }

    #URL: "/calc/costPresets"
    /*public function getAllPresets(string $searchkey, int $start = 0, int $count = 20)
    {
        if ($searchkey != "") {
            $sql = "SELECT * FROM calc_cost WHERE name like concat('%',:search,'%') LIMIT :min, :max";
            $return = $this->prepareDB($sql, array(":search"=>$searchkey,":min"=>$start,":max"=>$count));
        } else {
            $sql = "SELECT * FROM calc_cost LIMIT :min, :max";
            $return = $this->prepareDB($sql, array(":min"=>$start, ":max"=>$count));
        }

        return array($return, null);
    }*/

    /*public function detailOnePreset(int $id)
    {
        $return = $this->prepareDB("SELECT * FROM calc_cost WHERE id = :id", array(":id"=>$id), true);

        if ($return) {
            return array($return, null);
        } else {
            return array(null, array("err_nr"=>"430", "err_msg" => "unknown id"));
        }
    }*/

    /*public function deleteOnePreset(int $id)
    {
        $this->noreturnDB("DELETE FROM calc_cost WHERE id = :id", array(":id"=>$id));
    }*/

    #URL: "/calc/costPresets/add"
    public function addOneCostPresets(string $name)
    {
        if ($name != "") {
            $this->prepareDB("INSERT INTO calc_cost (name) VALUES (:name)", array(":name" => $name));
    
            $return = $this->prepareDB("SELECT id, name FROM calc_cost 
            WHERE name = :name", array(":name" => $name), true);

            $returnData = array("costPresetId" => $return["id"],"name" => $return["name"], "rows" => array());

            return array($returnData, null);
        } else {
            return array(null, array("err_nr"=>"400", "err_msg" => "name not set"));
        }
    }

    #URL: "/calc/voucherPresets"
    public function getAllVoucherPresets(string $searchkey, int $start = 0, int $count = 20)
    {
        if ($searchkey != "") {
            $sql = "SELECT (SELECT COUNT(calc_voucher.id) FROM calc_voucher LEFT JOIN calc_voucher_presets ON 
            calc_voucher.id = calc_voucher_presets.calc_voucher_id) AS total,calc_voucher.id AS voucherPresetId,
            calc_voucher.name,calc_voucher.totalValue, calc_voucher_presets.id AS voucherRowId,
            calc_voucher_presets.name AS voucherRowName,calc_voucher_presets.calculationData FROM calc_voucher 
            LEFT JOIN calc_voucher_presets ON  calc_voucher.id = calc_voucher_presets.calc_voucher_id WHERE 
            calc_voucher.name like concat('%',:search,'%') ORDER BY calc_voucher.id ASC LIMIT :min, :max ";

            $return = $this->prepareDB($sql, array(":search"=>$searchkey,":min"=>$start,":max"=>$count));
        } else {
            $sql = "SELECT (SELECT COUNT(calc_voucher.id) FROM calc_voucher LEFT JOIN calc_voucher_presets ON 
            calc_voucher.id = calc_voucher_presets.calc_voucher_id) AS total,calc_voucher.id AS voucherPresetId, 
            calc_voucher.name,calc_voucher.totalValue, calc_voucher_presets.id AS voucherRowId,
            calc_voucher_presets.name AS voucherRowName,calc_voucher_presets.calculationData FROM calc_voucher 
            LEFT JOIN calc_voucher_presets ON calc_voucher.id = calc_voucher_presets.calc_voucher_id ORDER BY 
            calc_voucher.id ASC LIMIT :min, :max ";

            $return = $this->prepareDB($sql, array(":min" => $start, ":max" => $count));
        }
        $returnData = array("total"=>$return[0]["total"],"start"=>$start,"count"=>$count,"rows"=>array());

        $voucherPresetId = "";
        foreach ($return as $data) {
            if ($data["voucherPresetId"] != $voucherPresetId) {
                $returnData["rows"][] = ["voucherPresetId" => $data["voucherPresetId"],
                                         "name" => $data["name"],
                                         "totalValue" => $data["totalValue"],
                                         "rows" => [array("voucherRowId" => $data["voucherRowId"],
                                                         "name" => $data["voucherRowName"],
                                                         "calculationData" => $data["calculationData"])]];
                
                $voucherPresetId = $data["voucherPresetId"];
            } else {
                $returnData["rows"][count($returnData["rows"])-1]["rows"][] =
                array("voucherRowId" => $data["voucherRowId"],
                      "name" => $data["voucherRowName"],
                      "calculationData" => $data["calculationData"]);
            }
        }
        return array($returnData, null);
    }

    #URL: "/calc/voucherPresets/add"
    public function addOneVoucherPresets(string $name)
    {
        if ($name != "") {
            $this->prepareDB("INSERT INTO calc_voucher (name, totalValue) VALUES (:name,'[]')", array(":name" => $name));
    
            $return = $this->prepareDB("SELECT id, name, totalValue FROM calc_voucher 
            WHERE name = :name", array(":name" => $name), true);

            $returnData = array("voucherPresetId" => $return["id"],"name" => $return["name"],
            "totalValue" => $return["totalValue"], "rows" => array());

            return array($returnData, null);
        } else {
            return array(null, array("err_nr"=>"400", "err_msg" => "name not set"));
        }
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%"
    public function getOneVoucherPresets(int $id)
    {
        $sql = "SELECT calc_voucher.id AS voucherPresetId, calc_voucher.name AS voucherPresetName,
        calc_voucher.totalValue,calc_voucher_presets.id AS voucherRowId, calc_voucher_presets.name 
        AS VoucherRowName, calc_voucher_presets.calculationData  FROM `calc_voucher` LEFT JOIN 
        calc_voucher_presets ON calc_voucher.id = calc_voucher_presets.calc_voucher_id WHERE calc_voucher.id = :id";

        $return = $this->prepareDB($sql, array(":id" => $id));

        if ($return) {
            $returnData = array("voucherPresetId" => $return[0]["voucherPresetId"],
            "name" => $return[0]["voucherPresetName"], "totalValue" => $return[0]["totalValue"], "rows" => array());

            foreach ($return as $data) {
                $returnData["rows"][] = ["voucherRowId" => $data["voucherRowId"], "name" => $data["VoucherRowName"],
                "calculationData" => $data["calculationData"]];
            }

            return array($returnData, null);
        } else {
            return array(null, array("err_nr"=>"440", "err_msg" => "unknown id"));
        }
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%/update"
    public function updateOneVoucherPresets(int $id, string $name, string $totalValue)
    {
        if ($name != "") {
            $check = $this->getOneVoucherPresets($id);

            if ($check[1]["err_nr"] == null) {
                if ($totalValue != "") {
                    $sql = "UPDATE calc_voucher SET name = :name, totalValue = :totalValue WHERE id = :id";
                    $this->noreturnDB($sql, array(":id" => $id, ":name" => $name, ":totalValue" => $totalValue));
                } else {
                    $sql = "UPDATE calc_voucher SET name = :name WHERE id = :id";
                    $this->noreturnDB($sql, array(":id" => $id, ":name" => $name));
                }
    
                return $this->getOneVoucherPresets($id);
            } else {
                return array(null, array("err_nr"=>"450", "err_msg" => "unknown id!"));
            }
        } else {
            return array(null, array("err_nr"=>"400", "err_msg" => "name not set"));
        }
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%/delete"
    public function deleteOneVoucherPresets(int $id)
    {
        $sql = "DELETE calc_voucher_presets.*, calc_voucher.* FROM calc_voucher_presets LEFT JOIN calc_voucher ON
         calc_voucher_presets.calc_voucher_id = calc_voucher.id WHERE calc_voucher_id = :id";
        $this->noreturnDB($sql, array(":id" => $id));

        return array(array(),null);
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%/rows"
    public function getDetailVoucherPresets(int $id)
    {
        return $this->getOneVoucherPresets($id);
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%/rows/add"
    public function addVoucherPresetRow(int $id, string $name, string $calculationData)
    {
        if ($name != "") {
            $check = $this->getOneVoucherPresets($id);

            if ($check[1]["err_nr"] == null) {
                if ($calculationData != "") {
                    $sql = "INSERT INTO calc_voucher_presets (calc_voucher_id, name, calculationData)
                    VALUES (:id, :name, :calculationData)";
                    $this->noreturnDB($sql, array(":id" => $id, ":name" => $name, ":calculationData" =>
                    $calculationData));
                } else {
                    $sql = "INSERT INTO calc_voucher_presets (calc_voucher_id, name, calculationData) VALUES (:id, :name)";
                    $this->noreturnDB($sql, array(":id" => $id, ":name" => $name, "[]"));
                }

                return $this->getOneVoucherPresets($id);
            } else {
                return array(null, array("err_nr"=>"460", "err_msg" => "unknown id!"));
            }
        } else {
            return array(null, array("err_nr"=>"400", "err_msg" => "name not set"));
        }
    }

    public function getOneVoucherPresentRow(int $rid)
    {
        $sql = "SELECT id AS voucherRowId, name, calculationData FROM calc_voucher_presets WHERE id = :rid";
        $return = $this->prepareDB($sql, array(":rid" => $rid));

        if ($return) {
            return array($return, null);
        } else {
            return array(null, array("err_nr"=>"431", "err_msg" => "unknown RowId!"));
        }
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%/rows/%voucherRowId%/update"
    public function updateOneVoucherPresetRow(int $id, int $rid, string $name, string $calculationData)
    {
        if ($name != "") {
            $check = $this->getOneVoucherPresets($id);

            if ($check[1]["err_nr"] == null) {
                $check = $this->getOneVoucherPresentRow($rid);

                if ($check[1]["err_nr"] == null) {
                    if ($calculationData != "") {
                        $sql = "UPDATE calc_voucher_presets SET name = :name, calculationData = :calculationData WHERE 
                        id = :id";
                        $this->noreturnDB($sql, array(":id" => $rid, ":name" => $name, ":calculationData" =>
                        $calculationData));
                    } else {
                        $sql = "UPDATE calc_voucher_presets SET name = :name WHERE id = :id";
                        $this->noreturnDB($sql, array(":id" => $rid, ":name" => $name));
                    }
        
                    return $this->getOneVoucherPresentRow($rid);
                } else {
                    return array(null, array("err_nr"=>"431", "err_msg" => "unknown RowId!"));
                }
            } else {
                return array(null, array("err_nr"=>"470", "err_msg" => "unknown id!"));
            }
        } else {
            return array(null, array("err_nr"=>"400", "err_msg" => "name not set"));
        }
    }

    #URL: "/calc/voucherPresets/%voucherPresetId%/rows/%voucherRowId%/delete"
    public function deleteOneVoucherPresentRow($rid)
    {
        $sql = "DELETE calc_voucher_presets.* FROM calc_voucher_presets WHERE id = :rid";
        $this->noreturnDB($sql, array(":rid" => $rid));

        return array(array(),null);
    }
}
