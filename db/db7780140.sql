-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Erstellungszeit: 21. Mrz 2019 um 16:09
-- Server-Version: 5.7.24
-- PHP-Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db7780140`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `calc_cost`
--

DROP TABLE IF EXISTS `calc_cost`;
CREATE TABLE IF NOT EXISTS `calc_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `calc_cost`
--

INSERT INTO `calc_cost` (`id`, `name`) VALUES
(1, 'null');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `calc_info`
--

DROP TABLE IF EXISTS `calc_info`;
CREATE TABLE IF NOT EXISTS `calc_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `costPresetId` int(11) DEFAULT NULL,
  `voucherPresetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `costPresetId` (`costPresetId`),
  KEY `voucherPresetId` (`voucherPresetId`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `calc_info`
--

INSERT INTO `calc_info` (`id`, `name`, `costPresetId`, `voucherPresetId`) VALUES
(3, 'sddaw', NULL, 1),
(4, 'dwa', 1, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `calc_products`
--

DROP TABLE IF EXISTS `calc_products`;
CREATE TABLE IF NOT EXISTS `calc_products` (
  `calc_id` int(11) NOT NULL,
  `product_name` varchar(60) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_count` int(11) NOT NULL,
  PRIMARY KEY (`calc_id`,`product_name`),
  KEY `calc_id` (`calc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `calc_products`
--

INSERT INTO `calc_products` (`calc_id`, `product_name`, `product_price`, `product_count`) VALUES
(3, 't1', 20, 1),
(3, 't2', 2, 32);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `calc_voucher`
--

DROP TABLE IF EXISTS `calc_voucher`;
CREATE TABLE IF NOT EXISTS `calc_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `calc_voucher`
--

INSERT INTO `calc_voucher` (`id`, `name`) VALUES
(1, 'null');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_data`
--

DROP TABLE IF EXISTS `user_data`;
CREATE TABLE IF NOT EXISTS `user_data` (
  `userID` int(11) NOT NULL,
  `userName` varchar(40) NOT NULL,
  `screenName` varchar(40) NOT NULL,
  `email` varchar(60) NOT NULL,
  `userPassword` varchar(10) NOT NULL,
  PRIMARY KEY (`userName`),
  UNIQUE KEY `userID` (`userID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user_data`
--

INSERT INTO `user_data` (`userID`, `userName`, `screenName`, `email`, `userPassword`) VALUES
(1, 'test', 'TestUser', '', 'test');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_session`
--

DROP TABLE IF EXISTS `user_session`;
CREATE TABLE IF NOT EXISTS `user_session` (
  `userID` int(11) NOT NULL,
  `sessionStart` datetime NOT NULL,
  `sessionStop` datetime NOT NULL,
  `token` varchar(32) NOT NULL,
  `ip` varchar(32) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user_session`
--

INSERT INTO `user_session` (`userID`, `sessionStart`, `sessionStop`, `token`, `ip`) VALUES
(1, '2019-03-21 16:53:30', '2019-03-21 17:08:16', 'd034b19f39957edcbf05584c427949d5', '::1');

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `calc_info`
--
ALTER TABLE `calc_info`
  ADD CONSTRAINT `calc_info_ibfk_1` FOREIGN KEY (`costPresetId`) REFERENCES `calc_cost` (`id`),
  ADD CONSTRAINT `calc_info_ibfk_2` FOREIGN KEY (`voucherPresetId`) REFERENCES `calc_voucher` (`id`);

--
-- Constraints der Tabelle `calc_products`
--
ALTER TABLE `calc_products`
  ADD CONSTRAINT `calc_products_ibfk_1` FOREIGN KEY (`calc_id`) REFERENCES `calc_info` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
