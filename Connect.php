<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// Set CORS Headers
// array holding allowed Origin domains
$allowedOrigins = array(
  // '(http(s)://)?(www\.)?my\-domain\.com',
  '(http(s)://)?localhost(:\d+)?',
  '(http(s)://)?192.168.188.24(:\d+)?'
);
 
if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] != '') {
    foreach ($allowedOrigins as $allowedOrigin) {
        if (preg_match('#' . $allowedOrigin . '#', $_SERVER['HTTP_ORIGIN'])) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Max-Age: 1000');
                    header("Access-Control-Allow-Credentials: true");
                    header("Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT,DELETE");
                    header("Access-Control-Allow-Headers: Authorization, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
            break;
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    die();
}

header('Content-Type: application/json');

require_once("./inc/QueryData.php");
require_once("./inc/DatabaseObject.php");

use Calc\DatabaseObject;
use Calc\PlentyMarkets;
use Calc\QueryData;

$querys = new QueryData();
$db = DatabaseObject::instance();


function buildPrettyAnswer(?array $success, ?array $failure)
{
    $success = ($success == null) ? array() : $success;
    $failure = ($failure == null) ? array("err_nr"=> null, "err_msg" => "") : $failure;

    $fancyReturn = array("data" => $success, "error" => $failure);
    return json_encode($fancyReturn);
}

function convertPrettyAnswer(array $mixedAnswer)
{
    return buildPrettyAnswer($mixedAnswer[0], $mixedAnswer[1]);
}



if (isset($_SERVER["REQUEST_URI"])) {
    if ($querys->page == "heartbeat" && $querys->method == "GET") {
        if ($db->checkValidUser($querys->authToken)) {
            echo convertPrettyAnswer($db->refreshUser($querys->authToken));
            die();
        } else {
            echo buildPrettyAnswer(null, array("err_nr" => "401", "err_msg" => "authToken missing"));
            die();
        }
    }

    if ($querys->page == "user") {
        if ($querys->action == "login" && $querys->method == "POST") {
            if (isset($querys->params["username"]) && isset($querys->params["password"])) {
                echo(convertPrettyAnswer($db->loginUser($querys->params["username"], $querys->params["password"])));
                die();
            } else {
                echo buildPrettyAnswer(null, array("err_nr" => "406", "err_msg" => "wrong parameters"));
                die();
            }
        }
    
        if ($querys->action == "logoff" && $querys->method == "POST" || $querys->method == "GET") {
            if (isset($querys->authToken)) {
                echo convertPrettyAnswer($db->logoffUser($querys->authToken));
                die();
            } else {
                echo buildPrettyAnswer(null, array("err_nr" => "401", "err_msg" => "authToken missing"));
                die();
            }
        }
    }

    if ($querys->page== "calc") {
        if ($db->checkValidUser($querys->authToken)) {
            #URL: "/calc"
            if (($querys->action == "none") && ($querys->method == "GET" || $querys->method == "POST")) {
                $name = isset($querys->params["searchkey"]) ? $querys->params["searchkey"] : "";
                $start = isset($querys->params["start"]) ? $querys->params["start"] : 0;
                $count = isset($querys->params["count"]) ? $querys->params["count"] : 20;
                echo convertPrettyAnswer($db->getAllCalc($name, $start, $count));
                die();
            }

            #URL: "/calc/add"
            if (($querys->action == "add") && $querys->method == "POST") {
                if (isset($querys->params["name"])) {
                    $costPreset = isset($querys->params["costPresetId"]) ? $querys->params["costPresetId"] : null;
                    $voucherPreset = isset($querys->params["voucherPresetId"]) ?
                        $querys->params["voucherPresetId"] : null;
                    echo convertPrettyAnswer($db->addOneCalc($querys->params["name"], $costPreset, $voucherPreset));
                    die();
                } else {
                    echo buildPrettyAnswer(null, array("err_nr"=>"401", "err_mgs" => "name missing"));
                    die();
                }
            }

            #URL: "/calc/%id%"
            if (ctype_digit($querys->action) && empty($querys->params) &&$querys->method == "GET") {
                echo convertPrettyAnswer($db->detailOneCalc((int)$querys->action));
                die();
            }

            #URL: "/calc/%id%/update"
            if (ctype_digit($querys->action) &&  array_key_exists("update", $querys->params) &&
            ($querys->params["update"] == null) && !array_key_exists("products", $querys->params) &&
            $querys->method == "POST") {
                $name = isset($querys->params["name"]) ? $querys->params["name"] : "";
                $costPreset = isset($querys->params["costPresetId"]) ? $querys->params["costPresetId"] : null;
                $voucherPreset = isset($querys->params["voucherPresetId"]) ? $querys->params["voucherPresetId"] : null;
                echo convertPrettyAnswer($db->updateOneCalc((int)$querys->action, $name, $costPreset, $voucherPreset));
                die();
            }

            #URL: "/calc/%id%/delete"
            if (ctype_digit($querys->action) && array_key_exists("delete", $querys->params) &&
            ($querys->params["delete"] == null) && !array_key_exists("products", $querys->params) &&
            $querys->method == "DELETE") {
                echo convertPrettyAnswer($db->deleteOneCalc((int)$querys->action));
                die();
            }

            #URL: "/calc/%id%/products"
            if (ctype_digit($querys->action) && array_key_exists("products", $querys->params) &&
            ($querys->params["products"] == null) && $querys->method == "GET") {
                $name = isset($querys->params["searchkey"]) ? $querys->params["searchkey"] : "";
                $start = isset($querys->params["start"]) ? $querys->params["start"] : 0;
                $count = isset($querys->params["count"]) ? $querys->params["count"] : 20;
                //TBD
                echo convertPrettyAnswer($db->getAllProductsOneCalc((int)$querys->action, $name, $start, $count));
                die();
            }

            #URL: "/calc/%id%/products/add"
            if (ctype_digit($querys->action) && array_key_exists("products", $querys->params) &&
            ($querys->params["products"] == "add") && $querys->method == "POST") {
                if (isset($querys->params["plentyId"]) && $querys->params["plentyId"] != null) {
                    echo convertPrettyAnswer(
                        $db->addOneProductOneCalc((int)$querys->action, (int)$querys->params["plentyId"])
                    );
                    die();
                } else {
                    echo buildPrettyAnswer(null, array("err_nr"=>"401", "err_mgs" => "plentyId missing"));
                    die();
                }
            }

            #URL: "/calc/%id%/products/%productId%"
            if (ctype_digit($querys->action) && array_key_exists("products", $querys->params) &&
            ctype_digit($querys->params["products"]) && $querys->method == "GET") {
                //TBD
                echo convertPrettyAnswer($db->updateOneProduct((int)$querys->action, 1));
                die();
            }

            #URL: "/calc/%id%/products/%productId%/update"
            if (ctype_digit($querys->action) && array_key_exists("products", $querys->params) &&
            ctype_digit($querys->params["products"]) && array_key_exists("update", $querys->params) &&
            ($querys->params["update"] == null) && $querys->method == "POST") {
                //TBD
                echo convertPrettyAnswer($db->detailOneProduct((int)$querys->action, 1));
                die();
            }

            #URL: "/calc/%id%/products/%productId%/delete"
            if (ctype_digit($querys->action) && array_key_exists("products", $querys->params) &&
            ctype_digit($querys->params["products"]) && array_key_exists("delete", $querys->params) &&
            ($querys->params["delete"] == null) && $querys->method == "DELETE") {
                //TBD
                echo convertPrettyAnswer($db->delteOneProduct((int)$querys->action, 1));
                die();
            }

            #URL: "/calc/products"
            if ($querys->action == "products" && ($querys->method == "GET" || $querys->method == "POST")) {
                //TDB
                $name = isset($querys->params["searchkey"]) ? $querys->params["searchkey"] : "";
                $start = isset($querys->params["start"]) ? $querys->params["start"] : 0;
                $count = isset($querys->params["count"]) ? $querys->params["count"] : 20;
                echo convertPrettyAnswer($db->getAllProductPrice($name, $start, $count));
                die();
            }

            #URL: "/calc/productGroups"
            if ($querys->action == "productGroups" && ($querys->method == "GET" || $querys->method == "POST")) {
                echo convertPrettyAnswer($db->getAllGroups());
                die();
            }

            #URL: "/calc/costPresets"
            if ($querys->action == "costPresets" && (reset($querys->params) == null) &&
            empty($querys->params) && ($querys->method == "GET" || $querys->method == "POST")) {
                $name = isset($querys->params["searchkey"]) ? $querys->params["searchkey"] : "";
                $start = isset($querys->params["start"]) ? $querys->params["start"] : 0;
                $count = isset($querys->params["count"]) ? $querys->params["count"] : 20;
                echo convertPrettyAnswer($db->getAllPresets($name, $start, $count));
                die();
            }
            
            #URL: "/calc/costPresets/add"
            if ($querys->action == "costPresets" && array_key_exists("add", $querys->params) &&
            ($querys->method == "POST")) {
                $name = isset($querys->params["name"]) ? $querys->params["name"] : "";
                echo convertPrettyAnswer($db->addOneCostPresets($name));
                die();
            }

            #URL: "/calc/costPresets/%costPresetId%"
            if ($querys->action == "costPresets" && (reset($querys->params) == null) &&
            ($querys->method == "GET" || $querys->method == "POST")) {
                echo convertPrettyAnswer($db->detailOnePreset((int)key($querys->params)));
                die();
            }

            #URL: "/calc/costPresets/%costPresetId%/update"
            if ($querys->action == "costPresets" && (reset($querys->params) == "update") &&
            ($querys->method == "GET" || $querys->method == "POST")) {
                echo convertPrettyAnswer($db->detailOnePreset((int)key($querys->params)));
                die();
            }

            #URL: "/calc/costPresets/%costPresetId%/delete"
            if ($querys->action == "costPresets" && (reset($querys->params) == "delete") &&
            $querys->method == "DELETE") {
                $db->deleteOnePreset((int)key($querys->params));
                echo buildPrettyAnswer(null, null);
                die();
            }

            #URL: "/calc/costPresets/%costPresetId%/rows"
            if ($querys->action == "costPresets" && (reset($querys->params) == "rows") &&
            !array_key_exists("add", $querys->params) && $querys->method == "GET") {
                echo("costPresetsIDRows");
                die();
            }

            #URL: "/calc/costPresets/%costPresetId%/rows/add"
            if ($querys->action == "costPresets" && (reset($querys->params) == "rows") &&
            array_key_exists("add", $querys->params) && $querys->method == "POST") {
                echo("costPresetsIDRowsADD");
                die();
            }

            #URL: "/calc/costPresets/%costPresetId%/rows/%costRowId%/update"
            if ($querys->action == "costPresets" && (reset($querys->params) == "rows") &&
            (end($querys->params) == "update") && $querys->method == "POST") {
                echo("costPresetsIDRowsUpdate");
                die();
            }
  
            #URL: "/calc/costPresets/%costPresetId%/rows/%costRowId%/delete"
            if ($querys->action == "costPresets" && (reset($querys->params) == "rows") &&
            (end($querys->params) == "delete") && $querys->method == "DELETE") {
                echo("costPresetsIDRowsDelete");
                die();
            }

            #URL: "/calc/voucherPresets"
            if ($querys->action == "voucherPresets" && (empty($querys->params) ||
            isset($querys->params["searchkey"]) || isset($querys->params["start"]) ||
            isset($querys->params["count"])) && ($querys->method == "GET" || $querys->method == "POST")) {
                $name = isset($querys->params["searchkey"]) ? $querys->params["searchkey"] : "";
                $start = isset($querys->params["start"]) ? $querys->params["start"] : 0;
                $count = isset($querys->params["count"]) ? $querys->params["count"] : 20;
                echo convertPrettyAnswer($db->getAllVoucherPresets($name, $start, $count));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/add"
            if ($querys->action == "voucherPresets" && array_key_exists("add", $querys->params) &&
            ($querys->method == "POST")) {
                $name = isset($querys->params["name"]) ? $querys->params["name"] : "";
                echo convertPrettyAnswer($db->addOneVoucherPresets($name));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%"
            if ($querys->action == "voucherPresets" && empty(reset($querys->params)) &&
            ($querys->method == "GET")) {
                echo convertPrettyAnswer($db->getOneVoucherPresets((int)key($querys->params)));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/update"
            if ($querys->action == "voucherPresets" && !array_key_exists("rows", $querys->params) &&
            (in_array("update", $querys->params) && array_key_exists("name", $querys->params)) &&
            ($querys->method == "POST")) {
                $name = isset($querys->params["name"]) ? $querys->params["name"] : "";
                $totalValue = isset($querys->params["totalValue"]) ? $querys->params["totalValue"] : "";
                $id = (int)key(array_slice($querys->params, -1, null, true));
                echo convertPrettyAnswer($db->updateOneVoucherPresets($id, $name, $totalValue));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/delete"
            if ($querys->action == "voucherPresets" && (reset($querys->params) == "delete") &&
            ($querys->method == "DELETE")) {
                echo convertPrettyAnswer($db->deleteOneVoucherPresets((int)key($querys->params)));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/rows"
            if ($querys->action == "voucherPresets" && (in_array("rows", $querys->params)) && (
            !array_key_exists("add", $querys->params)) && !in_array("update", $querys->params) &&
            ($querys->method == "GET")) {
                echo convertPrettyAnswer($db->getDetailVoucherPresets((int)key($querys->params)));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/rows/add"
            if ($querys->action == "voucherPresets" &&
            (array_key_exists("add", $querys->params) && array_key_exists("name", $querys->params)) &&
            ($querys->method == "POST")) {
                $name = isset($querys->params["name"]) ? $querys->params["name"] : "";
                $calculationData = isset($querys->params["calculationData"]) ? $querys->params["calculationData"] : "";
                $id = (int)key(array_slice($querys->params, -2, null, true));
                echo convertPrettyAnswer($db->addVoucherPresetRow($id, $name, $calculationData));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/rows/%voucherRowId%/update"
            if ($querys->action == "voucherPresets" &&
            (in_array("update", $querys->params) && in_array("rows", $querys->params)) &&
            $querys->method == "POST") {
                $name = isset($querys->params["name"]) ? $querys->params["name"] : "";
                $calculationData = isset($querys->params["calculationData"]) ? $querys->params["calculationData"] : "";
                $id = (int)key(array_slice($querys->params, -2, null, true));
                $rid = (int)key(array_slice($querys->params, -1, null, true));
                echo convertPrettyAnswer($db->updateOneVoucherPresetRow($id, $rid, $name, $calculationData));
                die();
            }

            #URL: "/calc/voucherPresets/%voucherPresetId%/rows/%voucherRowId%/delete"
            if ($querys->action == "voucherPresets" &&
            (reset($querys->params) == "rows") && (end($querys->params) == "delete")
            && $querys->method == "DELETE") {
                $rid = (int)key(array_slice($querys->params, -1, null, true));
                echo convertPrettyAnswer($db->deleteOneVoucherPresentRow($rid));
                die();
            }
        } else {
            echo(json_encode(array("err_nr"=>"400", "err_msg" => "wrong authentication")));
            die();
        }
    }
    echo buildPrettyAnswer(null, array("err_nr" => "404", "error_msg" => "unknown base url"));
}
